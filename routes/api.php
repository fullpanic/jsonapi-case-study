<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', 'JWTAuthController@register');
    Route::post('login', 'JWTAuthController@login');
    Route::post('logout', 'JWTAuthController@logout');
});

Route::resource('categories', 'CategoryController', [ 'only' => ['index', 'show'] ]);

Route::middleware(['auth:api'])->group(function () {
    Route::get('me', 'JWTAuthController@me');

    Route::resource('items', 'ItemController');
    Route::post('items/{id}/book', 'ItemController@book');
});
