<?php

namespace App\Http\Controllers;

use App\Http\Controllers\JsonApiController;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends JsonApiController
{
    public function __construct()
    {
        $this->model = Category::class;
        $this->authorize = ['create', 'update', 'delete'];
    }
}
