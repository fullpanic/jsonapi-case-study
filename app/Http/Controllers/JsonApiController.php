<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\StandardResource;
use App\Http\Resources\StandardCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Log;
use DB;

/**
 * A custom controller class that handles json requests that conform to the JSONAPI specification.
 * read more at: https://jsonapi.org/format/
 */
class JsonApiController extends Controller
{
    /**
     * Class of the model that the controller manages.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * array specifying which abilities need authorizaion.
     *
     * @var array
     */
    protected $authorize = [];

    /**
     * @override
     * extends the functionality of the default authorize function.
     *
     * @return array
     */
    public function authorize($ability, $arguments = [])
    {
        if (array_search($ability, $this->authorize)) {
            try {
                parent::authorize($ability, $arguments);
            } catch (\Illuminate\Auth\Access\AuthorizationException $ex) {
                $baseClass = class_basename($arguments);
                throw new \Illuminate\Auth\Access\AuthorizationException("Unauthorized to {$ability} {$baseClass}");
            }
        }
    }

    /**
     * Return a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', $this->model);

        $query = $this->model::whereRaw('1 = 1');
        $this->filter($request, $query);
        $this->include($request, $query);
        $this->sort($request, $query);
        $objects = $this->fetch($request, $query);

        return new StandardCollection($objects);
    }

    /**
     * return a resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $object = $this->model::find($id);

        if ($object === null) {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException(class_basename($this->model . ' not found.'));
        }

        $this->authorize('view', $object);

        $this->include($request, $object);

        return new StandardResource($object);
    }

    /**
     * Validate the store request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateStore(Request $request) {
        return;
    }

    /**
     * Store a resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->model);
        $this->validateStore($request);

        $attributes = $request->input('data.attributes');
        $object = new $this->model();
        $object->fill($attributes);

        DB::beginTransaction();

        $object->save();
        
        $relationships = $request->input('data.relationships', []);
        // loop over provided relationships and get their name and provided data
        foreach ($relationships as $relationshipName => $relationship) {
            // create a relationship instance
            $relInstance = $object->$relationshipName();
            if ($relInstance instanceof BelongsToMany || $relInstance instanceof HasMany) {
                // get related IDs
                $ids = array_pluck($relationship['data'], 'id');
                // get models from IDs
                $relatedObjects = $relInstance->getModel()::whereIn('id', $ids)->get();
                // check if user is authorized to create relationships to those models.
                foreach ($relatedObjects as $relatedObject) {
                    $this->authorize('view', $relatedObject);
                }
                $relInstance->saveMany($relatedObjects);
            }
        }

        DB::commit();

        return new StandardResource($object);
    }

    /**
     * Validate the update request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateUpdate(Request $request) {
        return;
    }

    /**
     *
     * Update a resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = $this->model::find($id);

        $this->authorize('update', $object);
        $this->validateUpdate($request);

        if ($object) {
            $attributes = $request->input('data.attributes');
            $object->fill($attributes);
            $object->save();

            return new StandardResource($object);
        } else {
            return response()->json([], 404);
        }
    }

    /**
     *
     * Delete a resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = $this->model::find($id);

        if ($object === null) {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException(class_basename($this->model . ' not found.'));
        }

        $this->authorize('delete', $object);

        $object->delete();

        return response()->json([], 200);
    }

    // public function fetchRelationship(Request $request, $id, $relationship) {
    //     $object = $this->model::find($id);
    //     if ($object) {
    //         $relationship = $object->$relationship()->get();
    //         if (!$relationship) {
    //             return response()->json(['data' => null], 200);
    //         } else if ($relationship instanceof \Illuminate\Database\Eloquent\Collection) {
    //             return new StandardCollection($relationship);
    //         } else {
    //             return new StandardResource($relationship);
    //         }
    //     } else {
    //         return response()->json([], 404);
    //     }
    // }

    // public function updateRelationship(Request $request, $id, $relationship) {
    //     $object = $this->model::find($id);
    //     if ($object) {
    //         $relInstance = $object->$relationship();
    //         if ($relInstance instanceof BelongsToMany) {
    //             $ids = array_pluck($request->data, 'id');
    //             $relInstance->sync($ids);
    //         } else {
    //             return response()->json(['errors' => ['Not allowed to mass update relationship of type "' . get_class($relInstance) . '".']], 403);
    //         }
    //     } else {
    //         return response()->json([], 404);
    //     }
    // }
    
    /**
     * Adds filters from the request to the query.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Http\Response
     *
     * ex. of request url with a filter: https://example.com/api/foo?filter[bar]=eq:5&filter[baz]=like:wx,yz&filter[qux.quuz]=ne:10
     */
    private function filter(Request $request, $query)
    {
        $ruleTypeMap = [
            'eq' => '=',
            'ne' => '!=',
            'lt' => '<',
            'gt' => '>',
            'lte' => '<=',
            'gte' => '>=',
            'like' => 'like',
            'between' => 'between'
        ];

        $filters = $request->input('filter', []);
        $filters = array_merge($filters, $this->evaluateForcedFilters());

        foreach ($filters as $ruleSubject => $filter) {
            // parse filter string. ex: "eq:5,8"
            $ruleType = Str::before($filter, ':');
            $ruleObjects = explode(',', Str::after($filter, ':'));
            if ($ruleType === 'like') {
                // transform rule object for DB "LIKE" check. ex: "foo" -> "%foo%" 
                $ruleObjects = array_map(function ($element) {
                    return '%' . $element . '%';
                }, $ruleObjects);
            }

            // determine if the filter subject is on a related model
            $temp = $ruleSubject; // ex: "category.name"
            $ruleSubject = [];
            $temp = explode('.', $temp); 
            // last segment is the attribute
            $ruleSubject['attribute'] = array_pop($temp);
            // if more segments remain, concatinate them to reform relationship name
            if (count($temp) > 0) {
                $ruleSubject['relationship'] = implode('.', $temp);
            }
            
            if ($ruleType === 'in' || $ruleType === 'between') {
                $whereFunc = 'where' . ucfirst($ruleType);

                if (isset($ruleSubject['relationship'])) {
                    // to avoid column name ambiguity in query, get the related table name
                    $targetTableName = $this->getTargetTableName($ruleSubject['relationship']);

                    $query->whereHas($ruleSubject['relationship'], function ($query) use ($whereFunc, $ruleSubject, $ruleObjects, $targetTableName) {
                        $query->$whereFunc($targetTableName . '.' . $ruleSubject['attribute'], $ruleObjects);
                    });
                } else {
                    $query->$whereFunc($ruleSubject['attribute'], $ruleObjects);
                }

            } elseif ($ruleType === 'eq' || $ruleType === 'ne' || $ruleType === 'lt' ||
            $ruleType === 'gt' || $ruleType === 'lte' || $ruleType === 'gte' || $ruleType === 'like') {
                $ruleType = $ruleTypeMap[$ruleType];

                if (isset($ruleSubject['relationship'])) {
                    // to avoid column name ambiguity in query, get the related table name
                    $targetTableName = $this->getTargetTableName($ruleSubject['relationship']);
                    
                    $query->whereHas($ruleSubject['relationship'], function ($query) use ($ruleType, $ruleSubject, $ruleObjects, $targetTableName) {
                        $query->where(function ($query) use ($ruleType, $ruleSubject, $ruleObjects, $targetTableName) {
                            foreach ($ruleObjects as $ruleObject) {
                                $query->orWhere($targetTableName . '.' . $ruleSubject['attribute'], $ruleType, $ruleObject);
                            }
                        });
                    });
                } else {
                    $query->where(function ($query) use ($ruleType, $ruleSubject, $ruleObjects) {
                        foreach ($ruleObjects as $ruleObject) {
                            $query->orWhere($ruleSubject['attribute'], $ruleType, $ruleObject);
                        }
                    });
                }

            }
        }
    }

    /**
     * Get the table name of a relatonship name (dot seperated).
     *
     * @param string $relationshipName
     *
     * @return string
     *
     */
    private function getTargetTableName($relationshipName) {
        $model = new $this->model();
        foreach (explode('.', $relationshipName) as $name) {
            $model = $model->$name()->getModel();
        }
        return $model->getTable();
    }

    /**
     * Adds included relationships from the request to the query or model.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model $subject
     *
     * @return \Illuminate\Http\Response
     *
     * ex. of request url with a filter: https://example.com/api/foo?included=bar,baz
     *
     */
    private function include(Request $request, $subject)
    {
        if ($request->has('include')) {
            $rels = explode(',', $request->include);
            if ($subject instanceof \Illuminate\Database\Eloquent\Builder) {
                $subject->with($rels);
            } elseif ($subject instanceof \Illuminate\Database\Eloquent\Model) {
                foreach ($rels as $rel) {
                    $subject->load($rel);
                }
            }
        }
    }

    /**
     * Adds sorting from the request to the query.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Http\Response
     *
     * ex. of request url with a filter: https://example.com/api/foo?sort=bar,-baz
     */
    private function sort(Request $request, $query)
    {
        if ($request->has('sort')) {
            $items = explode(',', $request->sort);
            foreach ($items as $item) {
                if ($item[0] === '-') {
                    $query->orderBy(Str::after($item, '-'), 'desc');
                } else {
                    $query->orderBy($item, 'asc');
                }
            }
        }
    }

    /**
     * Adds pagination from the request and fetches the finalized query.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Http\Response
     */
    private function fetch(Request $request, $query)
    {
        if ($request->has('page.number')) {
            return $query->paginate($request->input('page.size', 20), ['*'], 'page', $request->input('page.number'));
        } else {
            return $query->get();
        }
    }

    /**
     * Defines filters that are forced on a listing request.
     *
     * @return array
     */
    protected function getForcedFilters()
    {
        return [];
    }

    /**
     * Evaluates forced filters.
     *
     * @return array
     */
    private function evaluateForcedFilters()
    {
        $evaluation = [];
        foreach ($this->getForcedFilters() as $key => $value) {
            $evaluation[$key] = is_callable($value) ? $value() : $value;
        }
        return $evaluation;
    }
}
