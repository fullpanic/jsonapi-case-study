<?php

namespace App\Http\Controllers;

use App\Http\Controllers\JsonApiController;
use App\Http\Resources\StandardResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Item;

class ItemController extends JsonApiController
{
    public function __construct()
    {
        $this->model = Item::class;
        $this->authorize = ['viewAny', 'view', 'create', 'update', 'delete'];
    }

    /**
     * Validate the store request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data.attributes.name' => ['required', 'string', 'min:11', 'max:256', 'not_regex:/free|offer|book|website/i'],
            'data.attributes.rating' => 'required|integer|min:0|max:5',
            'data.attributes.category_id' => 'required|integer|exists:categories,id',
            'data.attributes.location.country' => 'required|string|max:128',
            'data.attributes.location.state' => 'required|string|max:128',
            'data.attributes.location.city' => 'required|string|max:128',
            'data.attributes.location.zip_code' => 'required|digits:5',
            'data.attributes.location.address' => 'required|string|max:1024',
            'data.attributes.reputation' => 'required|integer|min:0|max:1000',
            'data.attributes.price' => 'required|integer|min:0',
            'data.attributes.availability' => 'required|integer|min:0',
            'data.attributes.image' => 'required|url',
        ], [ 'data.attributes.name.not_regex' => 'The :attribute must not contain the words "free", "offer", "book" or "website"' ]);
        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }
    }

    public function store(Request $request)
    {
        $this->authorize('create', $this->model);

        $this->validateStore($request);

        DB::beginTransaction();

        $data = $request->input('data.attributes');
        $data['user_id'] = auth()->user()->id;

        $item = Item::create(Arr::except($data, ['location']));

        $item->location()->create($data['location']);

        DB::commit();

        $item->load('location');
        $item->load('category');

        return new StandardResource($item);
    }

    /**
     * Validate the update request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data.attributes.name' => ['string', 'min:11', 'max:256', 'not_regex:/free|offer|book|website/i'],
            'data.attributes.rating' => 'integer|min:0|max:5',
            'data.attributes.category_id' => 'integer|exists:categories,id',
            'data.attributes.location.country' => 'string|max:128',
            'data.attributes.location.state' => 'string|max:128',
            'data.attributes.location.city' => 'string|max:128',
            'data.attributes.location.zip_code' => 'digits:5',
            'data.attributes.location.address' => 'max:1024',
            'data.attributes.reputation' => 'integer|min:0|max:1000',
            'data.attributes.price' => 'integer|min:0',
            'data.attributes.availability' => 'integer|min:0',
            'data.attributes.image' => 'url',
        ], [ 'data.attributes.name.not_regex' => 'The :attribute must not contain the words "free", "offer", "book" or "website"' ]);
        if ($validator->fails()) {
            throw new \Illuminate\Validation\ValidationException($validator);
        }
    }

    public function update(Request $request, $id)
    {
        $item = $this->model::find($id);

        if ($item === null) {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException(class_basename($this->model . ' not found.'));
        }

        $this->authorize('update', $item);

        $this->validateUpdate($request);

        DB::beginTransaction();

        $data = $request->input('data.attributes');
        $data['user_id'] = auth()->user()->id;

        $item->fill(Arr::except($data, ['location']));
        $item->save();

        if (isset($data['location']) && !empty($data['location'])) {
            $item->location->fill($data['location']);
            $item->location->save();
        }

        DB::commit();

        $item->load('location');
        $item->load('category');

        return new StandardResource($item);
    }

    public function book(Request $request, $id)
    {
        $item = $this->model::find($id);

        if ($item === null) {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException(class_basename($this->model . ' not found.'));
        }

        $this->authorize('update', $item);

        if ($item->availability <= 0) {
            return response()->json([
                'title' => 'Booking Error',
                'detail' => "The item \"{$item->name}\" is no longer available",
                'status' => 406
            ], 406);
        }
        
        $item->availability--;
        $item->save();
        return response()->json([], 200);
    }

    /**
     * @override
     * Defines filters that are forced on a listing request.
     *
     * @return array<any>
     */
    protected function getForcedFilters()
    {
        return [
            'user_id' => 'eq:' . auth()->user()->id
        ];
    }
}
