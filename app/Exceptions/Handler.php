<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
                return $this->errorResponse('Authentication Error', $exception->getMessage(), 401);
            } elseif ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
                return $this->errorResponse('Not Found Error', $exception->getMessage(), 404);
            } elseif ($exception instanceof \Illuminate\Auth\Access\AuthorizationException) {
                return $this->errorResponse('Authorization Error', $exception->getMessage(), 403);
            } elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
                return $this->errorResponse('Not Found Error', $exception->getMessage(), $exception->getStatusCode());
            } elseif ($exception instanceof \Illuminate\Validation\ValidationException) {
                return $this->errorResponse('Validation Error', $exception->getMessage(), $exception->status, ['errors' => $exception->errors()]);
            } elseif ($exception instanceof Exception) {
                return $this->errorResponse('Runtime Error', $exception->getMessage(), 500, ['trace' => $exception->getTrace()]);
            }
        }
        return parent::render($request, $exception);
    }

    public function errorResponse($title, $detail, $status, $extension = [])
    {
        return response()->json(array_merge([
            'title' => $title,
            'detail' => $detail,
            'status' => $status
        ], $extension), $status, ['Content-Type' => 'application/problem+json']);
    }
}
