<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public $appends = ['reputation_badge'];

    public function getReputationBadgeAttribute()
    {
        if ($this->reputation <= 500) {
            return 'red';
        } elseif ($this->reputation <= 799) {
            return 'yellow';
        } else {
            return 'green';
        }
    }

    /**
     * returns an instace of an item's relationship with its location.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function location()
    {
        return $this->hasOne(Location::class);
    }

    /**
     * returns an instace of an item's relationship with its category.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
