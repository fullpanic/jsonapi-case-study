<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (count(Category::all()) > 0) {
            return;
        }
        Category::insert(array_map(function ($element) {
            return ['name' => $element];
        }, ['hotel', 'alternative', 'hostel', 'lodge', 'resort', 'guest-house']));
    }
}
